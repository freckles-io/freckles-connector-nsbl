Collection of Ansible plugins used in nsbl/freckles.

Licenses
--------

For license/copyright information please check each of the files (or accompanying files with the same name but an appended '.license') seperately.

If no license information is provided for a file: copyright Markus Binsteiner, 2018, and license is the Parity Public License, version 3.0.0.
